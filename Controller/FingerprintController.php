<?php

namespace ATM\FingerprintBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use ATM\FingerprintBundle\Entity\API as FingerprintAPI;
use ATM\FingerprintBundle\Event\StatusError;
use ATM\FingerprintBundle\Event\StatusSuccess;

class FingerprintController extends Controller
{
    /**
     * @Route("/send/fingerprint", name="atm_fingerprint_send", options={"expose"=true})
     */
    public function saveFingerprintAction(){
        $config = $this->getParameter('atm_fingerprint_config');
        $request = $this->get('request_stack')->getCurrentRequest();
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $jsonData = $request->get('f');

        $data = json_decode($jsonData,true);
        $data['ip'] = $request->getClientIp();
        $data['site'] = $config['site_name'];
        $data['user_id'] = $user->getId();
        $data['username'] = $user->getUsername();
        $data['email'] = hash('sha512',$user->getEmail());
        $data['api_base_url'] = $config['api_base_url'];

        //CALL TO THE FINGERPRINT API
        if($config['active']){
            $response = FingerprintAPI::process($data);
        }else{
            $response = array('status' => 'ok');
        }

        $session = $this->get('session');
        $session->set('atm_fingerprint_sent',true);

        if($response['status'] == 'error'){
            $session->getFlashBag()->add('atm_fingerprint_fail_message',$config['error_message']);
            $session->set('atm_fingerprint_fail',$data);
            $session->set('atm_fingerprint_redirect',true);

            $event = new StatusError($user,$response,json_encode($data));
            $this->get('event_dispatcher')->dispatch(StatusError::NAME, $event);

            return new Response('ko');
        }else{

            $event = new StatusSuccess($user,$response,json_encode($data));
            $this->get('event_dispatcher')->dispatch(StatusSuccess::NAME, $event);

            return new Response('ok');
        }
    }

    /**
     * @Route("/store/fingerprint/code", name="atm_fingerprint_store_code")
     */
    public function getStoreFingerprintCodeAction(){
        $config = $this->getParameter('atm_fingerprint_config');

        return $this->render('ATMFingerprintBundle:Fingerprint:store_fingerprint.html.twig',array(
            'use_assetic' => $config['use_assetic'],
            'redirect' => $config['fingerprint_error_redirect_route_name']
        ));
    }
}
