A Fingerprint bundle to send data to the fingerprint api

## Installation ##

Install through composer:

```bash
php -d memory_limit=-1 composer.phar require atm/fingerprintbundle
```

In your AppKernel

```php
public function registerbundles()
{
    return [
    	...
    	...
    	new ATM\FingerprintBundle\ATMFingerprintBundle(),
    ];
}
```

## Configuration sample
Default values are shown below:
``` yml
# app/config/config.yml
  
atm_fingerprint:
    fingerprint_error_redirect_route_name: 'route to redirect the user when the fingerprint is not found'
    site_name: 'sitename'
    error_message: 'a message to show to the user when the fingerprint is not found'
    use_assetic: false
```

If you application uses assetic remember to include de ATMFingerprintBundle in the assetic array bundles in
your config.yml file:
``` yml
 assetic:
     bundles: ['ATMFingerprintBundle']
``` 

## Include to the main page
```twig
{% include 'ATMFingerprintBundle:Fingerprint:store_fingerprint.html.twig' %}
```

## Events

There are 2 events that you can use when the response from the API:
 - error:
```php
class StatusError extends Event{

    const NAME = 'atm_fingerprint_error.event';

    protected $user;
    protected $response;

    public function __construct($user,$response)
    {
        $this->user = $user;
        $this->response = $response;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getResponse(){
        return $this->response;
    }
}
```

- success:
```php
class StatusSuccess extends Event{

    const NAME = 'atm_fingerprint_success.event';

    protected $user;
    protected $response;

    public function __construct($user,$response)
    {
        $this->user = $user;
        $this->response = $response;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getResponse(){
        return $this->response;
    }
}
```

## Event Listener
There is an Event Listener that listens for kernel requests with a priority of 3 and sets a 
session variable called 'atm_fingerprint_custom_redirect'. 
```yml
ATM\FingerprintBundle\EventListener\FingerprintRedirect:
    autowire: true
    public: true
    tags:
        - { name: kernel.event_listener, event: kernel.request, method: onKernelRequest, priority: 3 }
    arguments:
        $atm_fingerprint_config: '%atm_fingerprint_config%'
```
You can set a custom request listener with a low priority to redirect to another place or to put some custom code.
Remember to remove the session variable 'atm_fingerprint_custom_redirect'
```yml
AppBundle\EventListener\CustomRequestListener:
    autowire: true
    public: true
    tags:
        - { name: kernel.event_listener, event: kernel.request, method: onKernelRequest, priority: 2 }
```
```php
namespace AppBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CustomRequestListener{

    private $session;

    public function __construct(SessionInterface $session){
        $this->session = $session;
    }
    
    public function onKernelRequest(GetResponseEvent $event){

        if($this->session->has('atm_fingerprint_custom_redirect')){
            $this->session->remove('atm_fingerprint_custom_redirect');
            
            // YOUR CODE GOES HERE
            
            $event->setResponse(new Response());
        }
    }
}
```
